from django import forms


from .models import Register
from .models import Contact

class RegisterForm(forms.ModelForm):
    class Meta:
        model = Register
        fields = ('name','email','number','file')

class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ('name','email','comment')


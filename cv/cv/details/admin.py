from django.contrib import admin

# Register your models here.

from .models import Register
from.models import Company_Detail
from.models import Contact
from.models import Hero_Page, About_Page


admin.site.register(Register)
admin.site.register(Contact)
admin.site.register(Company_Detail)
admin.site.register(Hero_Page)
admin.site.register(About_Page)

# Generated by Django 3.1.5 on 2021-01-12 09:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('details', '0002_auto_20210112_1521'),
    ]

    operations = [
        migrations.RenameField(
            model_name='register',
            old_name='number',
            new_name='numb',
        ),
    ]

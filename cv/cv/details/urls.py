from django.urls import path

from . import views

urlpatterns = [


    path('contact', views.ContactView.as_view(), name = 'contact' ),
    path('home', views.home, name='home'),

    path('register', views.RegisterView.as_view(), name='register'),
    path('', views.DetailView.as_view(), name='detail' ),



]
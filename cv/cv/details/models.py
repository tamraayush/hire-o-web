from django.db import models


# Create your models here.

class Register(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    number = models.IntegerField()
    file = models.FileField(upload_to='cv')

    def __str__(self):
        return self.name


class Contact(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    comment = models.TextField(max_length=200)

    def __str__(self):
        return self.name

class Company_Detail(models.Model):
    address = models.CharField(max_length=200)
    number = models.IntegerField()
    company_email= models.EmailField()

    def __str__(self):
        return self.address

class Hero_Page(models.Model):
    about= models.TextField(max_length=200)
    button = models.CharField(max_length=20)
    credential = models.TextField(max_length=100)

class About_Page(models.Model):
    about_header= models.CharField(max_length=30)
    about_box=models.TextField(max_length= 1000)
